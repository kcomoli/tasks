# Week 29 | July 13th to 17th

## What's keeping me busy
- MR approval + storage management

### Tasks
- [X] Start [UX Paid feature module content - merge requests approvals](https://gitlab.com/gitlab-org/growth/product/-/issues/37)
- [ ] Start [Broadcast message variant or new notification component](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/557)
- [X] Update IGP with 360 Feedback

### Unplanned tasks
- https://gitlab.com/gitlab-org/gitlab/-/issues/228655
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/36581

### Review needed

### Meetings
Doesn't include recurring company/stage group meetings and coffee chats.
- 1:1 Emily
- 1:1 Matej



