# Week 26 | June 22nd to 26th

## What's keeping me busy
- Onboarding
- 360 feedback
- Storage issue

### Tasks
- [X] Finish 360 feedback + self-review
- [X] Create follow-up issues for namespace storage
- [X] Follow-up https://gitlab.com/gitlab-org/gitlab/-/issues/220371/
- [X] Follow-up copy review for [Trial status](https://gitlab.com/gitlab-org/gitlab/-/issues/213376)  
- [X] Prepare handover for the trial status 

### Unplanned tasks
- [X] First round mock-ups for https://gitlab.com/gitlab-org/gitlab/-/issues/220371/
- [X] Review https://gitlab.com/gitlab-org/gitlab/-/merge_requests/35361
- [X] Review https://gitlab.com/gitlab-org/gitlab/-/merge_requests/35360

### Review needed
- [ ] Review https://gitlab.com/gitlab-org/gitlab/-/merge_requests/34083 -- Blocked

### Meetings
Doesn't include recurring company/stage group meetings and coffee chats.
- Enablement & Growth UX Team Meetup
- Catch up w/ Sam
- 1:1 Sunjung
