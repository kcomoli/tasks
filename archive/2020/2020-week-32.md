# Week 32 | August 3rd to 7th

## What's keeping me busy
- Follow up user testing recruitement
- Invited user emails

### Tasks
- [X] Create variants for https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/167
- [X] https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/161
- [X] Start Mapping out trial flow and sign-up flow https://gitlab.com/groups/gitlab-org/-/epics/3603 
- [X] Send recruitment emails
### Unplanned tasks
- [X] Review https://gitlab.com/gitlab-org/gitlab/-/merge_requests/38491
- [X] Review https://gitlab.com/gitlab-org/gitlab/-/merge_requests/38665#note_391094842
### Review needed

### Meetings
Doesn't include recurring company/stage group meetings and coffee chats.
- Enablement X Growth
- 1:1 Tim
- 1:1 Sam





