# Week 37 | September 7th to 11th

## What's keeping me busy
- Splitting time between Acquistion and Conversion
- Growth day 🙌

### Tasks
- [X] Start https://gitlab.com/gitlab-org/gitlab/-/issues/242009
- [X] Start https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/181
- [ ] Start https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/557
- [X] Create time tracking sheet and calculate E-factor
- [X] Update in app Upgrade flow
- [X] Create a feature grouping for https://gitlab.com/gitlab-org/ux-research/-/issues/1012

### Unplanned tasks


### Review needed


### Meetings
Doesn't include recurring company/stage group meetings and coffee chats.






    


