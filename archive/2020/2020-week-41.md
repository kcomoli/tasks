# Week 41 | October 5th to 9th

## What's keeping me busy
- Self-hosted trial wrap-up

### Tasks
- [X] Follow-up on https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/37
- [X] Define priority for https://gitlab.com/gitlab-org/ux-research/-/issues/1009
- [~] Progress on https://gitlab.com/gitlab-org/ux-research/-/issues/1012
- [~] Progress on https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/557
- [ ] Wrap-up https://gitlab.com/gitlab-org/gitlab/-/issues/230723
- [ ] Update issue decription for Broadcast component https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/557#note_422895818

### Unplanned tasks


### Review needed

### Meetings
Doesn't include recurring company/stage group meetings and coffee chats.
- 1:1 Emily
- 1:1 Matej




