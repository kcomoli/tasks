# Week 46 |  November 11 to 15

## What's keeping me busy

- Out November 11 and 15
- UX scorecard
- Paid sign up flow 


### Tasks
- [X] Update baseline JTBD New trial flow
- [X] Finish mobile version for the paid sign up flow.
- [X] Add feedback link on Security empty state.
- [ ] Add component design within pattern library to finish [MR accordions]( https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/merge_requests/1575).
- [ ] Start designing component for [trees](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/merge_requests/1582).

### Unplanned Tasks
- [X] Redesign the checkout process for the paid sign up flow  
### Meetings
Doesn't include recurring company/stage group meetings and coffee chats.

- [ ] UX/Acquisition/Conversion Planning - Postoponed
- [X] Growth & Fullfilment meeting.
