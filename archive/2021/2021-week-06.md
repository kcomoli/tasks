# Week 06 | Feburary 8th to 12th

## What's keeping me busy
- Continuous onboarding 
- Handover Activation

## Growth Projects

### Feature discovery

### Onboarding
- [X] https://gitlab.com/groups/gitlab-org/-/epics/4817
  * [X] Keep recruiting for user testing session
  * [X] Map overall strategy w/ mental model for Continuous onboarding

### Trials
- [X] Follow-up on https://gitlab.com/gitlab-org/gitlab/-/issues/273635
- [X] Refine discussion guide for problem validation https://gitlab.com/gitlab-org/ux-research/-/issues/1296
### Activation
- [X] Design proposal for https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/313
- [X] Prepare handover

## GitLab design

### Pajamas

## Misc
- [ ] Resume Install customer Dot dev env and link it to GDK https://gitlab.com/gitlab-org/customers-gitlab-com/-/blob/staging/doc/installation_steps.md 

### Review needed
- [X] https://gitlab.com/gitlab-org/gitlab/-/merge_requests/53259 

### Blocked 
- [ ] Wrap-up https://gitlab.com/gitlab-org/gitlab/-/issues/230723
- [ ] https://gitlab.com/gitlab-org/gitlab/-/issues/258983
- [ ] Keep working on JTBD for  https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/557#note_422895818

### Unplanned tasks
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/52660

## Admin and personal


## Meetings
Doesn't include recurring company/stage group meetings and coffee chats.
- User testing session
- Skip level Christie
- Enablement x Growth meeting
- 1:1 Matej
- Growth x Verify UX







