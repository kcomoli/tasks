# Week 46 | 15th - 18th November

## What's keeping me busy
- Weekly GitLab Stats Email interviews
- Reforge 
- OOO 11/19

## Growth Projects

### Conversion 
- [x] Follow-up  https://gitlab.com/gitlab-org/gitlab/-/issues/340948
- [x] Specs https://gitlab.com/groups/gitlab-org/-/epics/6901

#### PQLs
- [x] https://gitlab.com/gitlab-org/gitlab/-/issues/343177

#### Trials

#### Misc Experiments
- [-] Set-up sync meeting https://gitlab.com/gitlab-org/gitlab/-/issues/336467

### Adoption
- [x] Internal interviews https://gitlab.com/gitlab-org/gitlab/-/issues/259020

## GitLab design

### Pajamas
- [-] https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/1169

## Misc

### Review needed
- [x] Maintainer review for https://gitlab.com/gitlab-org/gitlab/-/merge_requests/72707


### Blocked 
- [ ] https://gitlab.com/gitlab-org/gitlab/-/issues/342097


### Unplanned tasks
- [x] https://gitlab.com/gitlab-org/gitlab/-/merge_requests/74551

## Admin and personal
- Start new Reforge program
- POC

## Meetings
Doesn't include recurring company/stage group meetings, coffee chats, design pairing session.
- Virtual Contribute
- 1:1 Mike
- 1:1 Matej
- Conv meeting
- Internal research session email x3



