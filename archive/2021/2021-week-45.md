# Week 45 | 8th - 12th November

## What's keeping me busy
- Weekly GitLab Stats Email
- Reforge 
- Guidelines empty states

## Growth Projects

### Conversion 
- [x]  Follow-up  https://gitlab.com/gitlab-org/gitlab/-/issues/340948
- [x] Wrap-up https://gitlab.com/groups/gitlab-org/-/epics/6901

#### PQLs
- [x] Keep working on https://gitlab.com/gitlab-org/gitlab/-/issues/340893 

#### Trials

#### Misc Experiments
- [-] Set-up sync meeting https://gitlab.com/gitlab-org/gitlab/-/issues/336467

### Adoption
- [x] Keep working on https://gitlab.com/gitlab-org/gitlab/-/issues/259020
- [x] https://gitlab.com/gitlab-org/gitlab/-/issues/342097

## GitLab design

### Pajamas
- [-] https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/1169
- [x] https://gitlab.com/gitlab-org/gitlab/-/merge_requests/72707

## Misc

### Review needed
- [x] https://gitlab.com/gitlab-org/gitlab/-/merge_requests/72707


### Blocked 


### Unplanned tasks


## Admin and personal
- Start new Reforge program
- POC

## Meetings
Doesn't include recurring company/stage group meetings, coffee chats, design pairing session.
- Skip level Christie
- Onboarding user interview
- 1:1 Emily B
- 1:1 Emily S
- 1:1 Tim

