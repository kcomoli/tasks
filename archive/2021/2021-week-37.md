# Week 37 | 13th September - 17th September

## What's keeping me busy
- First-Mile forks
- CO iteration

## Growth Projects

### Conversion 
- [x] Create survey for https://gitlab.com/gitlab-org/ux-research/-/issues/1572
- [x] Wrap-up https://gitlab.com/gitlab-org/gitlab/-/issues/336649 - Follow-up created https://gitlab.com/gitlab-org/gitlab/-/issues/340948

#### PQLs

#### Trials

#### Misc Experiments
- [x] Start https://gitlab.com/gitlab-org/gitlab/-/issues/336467

### Adoption
- [x] Wrap-up https://gitlab.com/gitlab-org/gitlab/-/issues/337978 

## GitLab design

### Pajamas

## Misc

### Review needed

### Blocked 
- [ ] https://gitlab.com/gitlab-org/gitlab/-/issues/258983.

### Unplanned tasks

## Admin and personal

## Meetings
Doesn't include recurring company/stage group meetings, coffee chats, design pairing session.
- 1:1 Emliy S
- 1:1 Emily B 
- 1:1 Tim

















