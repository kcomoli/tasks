# Week 48 | 30th November - 3rd December

## What's keeping me busy
- Weekly GitLab Stats Email interviews + summary
- Reforge 
- Cross stage feature discovery pages

## Growth Projects

### Conversion 


#### PQLs
- [x] https://gitlab.com/gitlab-org/gitlab/-/issues/343177

#### Trials

#### Misc Experiments
- [ ] Set-up sync meeting https://gitlab.com/gitlab-org/gitlab/-/issues/336467

### Adoption
- [x] Next steps for https://gitlab.com/gitlab-org/gitlab/-/issues/259020
- [x] Start https://gitlab.com/gitlab-org/gitlab/-/issues/343784/    

## GitLab design

### Pajamas
- [-] https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/1169 - Stretch

## Misc

### Review needed


### Blocked 
- [ ] https://gitlab.com/gitlab-org/gitlab/-/issues/342097


### Unplanned tasks

## Admin and personal
- Start new Reforge program

## Meetings
Doesn't include recurring company/stage group meetings, coffee chats, design pairing session.
- Sync Matej Reg flow + Design needs for Growth 
- EFG meetup
- 1:1 Mike





