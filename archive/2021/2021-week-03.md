# Week 02 | January 18th to 22nd

## What's keeping me busy
- Continuous onboarding user testing

## Growth Projects

### Feature discovery

### Onboarding
- [X] https://gitlab.com/groups/gitlab-org/-/epics/4817
  * [X] User testing session
  * [X] Recruit new user for testing session 

### Trials

### Activation
- [X] Generate specs for https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/268 based on feedback

## GitLab design

### Pajamas

## Misc
- [ ] Resume Install customer Dot dev env and link it to GDK https://gitlab.com/gitlab-org/customers-gitlab-com/-/blob/staging/doc/installation_steps.md 

### Review needed
- https://gitlab.com/gitlab-org/gitlab/-/merge_requests/51911

### Blocked 
- [ ] Wrap-up https://gitlab.com/gitlab-org/gitlab/-/issues/230723
- [ ] https://gitlab.com/gitlab-org/gitlab/-/issues/258983
- [ ] Keep working on JTBD for  https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/557#note_422895818

### Unplanned tasks


## Admin and personal
- [X] Review IGP goals


## Meetings
Doesn't include recurring company/stage group meetings and coffee chats.
- User testing Continuous onboarding
- 1:1 Dallas
- 1:1 Sam
- 1:1 Jacki
- Continuous onboarding MVC Kick-off




